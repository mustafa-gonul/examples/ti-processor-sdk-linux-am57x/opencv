#!/bin/bash

NAME=$(basename $(pwd))

export TI_OCL_KEEP_FILES=Y
export TI_OCL_LOAD_KERNELS_ONCHIP=Y
export TI_OCL_CACHE_KERNELS=Y

for IMAGE in ./images/*
do

  export OPENCV_OPENCL_DEVICE='TI AM57:ACCELERATOR:TI Multicore C66 DSP'

  echo ""
  echo "======================================================================================================"
  echo "OpenCL on, Exe: $NAME Image: $IMAGE"
  echo "======================================================================================================"
  ./"$NAME" "$IMAGE"
  echo ""

  export OPENCV_OPENCL_DEVICE='disabled'

  echo ""
  echo "======================================================================================================"
  echo "OpenCL off, Exe: $NAME Image: $IMAGE"
  echo "======================================================================================================"
  ./"$NAME" "$IMAGE"
  echo ""

done
