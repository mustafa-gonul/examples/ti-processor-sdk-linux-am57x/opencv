#!/bin/bash

# Configuration
. ../config.sh

# Variables
# Do not edit if you dont know what you are doing!
NAME=$(basename $(pwd))
DIR="/home/root/$NAME"
BUILD="build"
EXE="$BUILD/$NAME"


# Preparation for the build
[ ! -d "$BUILD" ] && mkdir "$BUILD"
[ -f "$EXE" ] && rm "$EXE"

# Build
arm-linux-gnueabihf-g++ -g -o "$EXE" main.cpp -lrt -lopencv_core -lopencv_imgproc -lopencv_video -lopencv_features2d -lopencv_imgcodecs -lopencv_photo
# lopencv_photo added for HDR

# Preparation for copying
ssh "$ENDPOINT" "[ -d $DIR ] && rm -Rf $DIR"
ssh "$ENDPOINT" "mkdir -p $DIR"
ssh "$ENDPOINT" "rm /tmp/opencl* 2> /dev/null"

# Copying
echo ""
echo "======================================================================================================"
echo "Preparing $NAME ..."
echo "======================================================================================================"
scp "$EXE"             "$ENDPOINT":"$DIR"

# If the example has its own images, code copies these and pass all of them once to the application
if [ -d ./images/ ]
then
  scp -r ./images/            "$ENDPOINT":"$DIR"
  scp ../common/pass-all.sh   "$ENDPOINT":"$DIR"/run.sh
else
  scp -r ../images/           "$ENDPOINT":"$DIR"
  scp ../common/pass-each.sh  "$ENDPOINT":"$DIR"/run.sh
fi

echo ""

# Running
ssh "$ENDPOINT" "cd $DIR && ./run.sh"
