#include <opencv2/photo.hpp>
#include "opencv2/imgcodecs.hpp"
#include <opencv2/highgui.hpp>

#include <time.h>
#include <unistd.h>

#include <vector>
#include <iostream>
#include <fstream>


/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

using namespace cv;
using namespace std;


void readImagesAndTimes(vector<UMat> &images, vector<float> &times)
{

  int numImages = 4;

  static const float timesArray[] = {1/30.0f,0.25,2.5,15.0};
  times.assign(timesArray, timesArray + numImages);

  static const char* filenames[] = {"./images/img_0.033.jpg", "./images/img_0.25.jpg", "./images/img_2.5.jpg", "./images/img_15.jpg"};
  for(int i=0; i < numImages; i++)
  {
    UMat im;
    imread(filenames[i], IMREAD_COLOR).copyTo(im);
    images.push_back(im);
  }

}

void run()
{
  struct timespec tp0, tp1, tp2, tp3, tp4, tp5, tp6, tp7;

  // Read images and exposure times
  // cout << "Reading images ... " << endl;
  vector<UMat> images;
  vector<Mat> output;
  vector<float> times;
  readImagesAndTimes(images, times);


  clock_gettime(CLOCK_MONOTONIC, &tp0);


  // Align input images
  // cout << "Aligning images ... " << endl;
  Ptr<AlignMTB> alignMTB = createAlignMTB();
  alignMTB->process(images, output);
  clock_gettime(CLOCK_MONOTONIC, &tp1);

  // Obtain Camera Response Function (CRF)
  // cout << "Calculating Camera Response Function (CRF) ... " << endl;

  Mat responseDebevec;
  Ptr<CalibrateDebevec> calibrateDebevec = createCalibrateDebevec();
  calibrateDebevec->process(images, responseDebevec, times);
  clock_gettime(CLOCK_MONOTONIC, &tp2);

  // Merge images into an HDR linear image
  // cout << "Merging images into one HDR image ... " ;

  Mat hdrDebevec;
  Ptr<MergeDebevec> mergeDebevec = createMergeDebevec();
  mergeDebevec->process(images, hdrDebevec, times, responseDebevec);
  clock_gettime(CLOCK_MONOTONIC, &tp3);

  // Save HDR image.
  // imwrite("hdrDebevec.hdr", hdrDebevec);
  // cout << "saved hdrDebevec.hdr "<< endl;

  // Tonemap using Drago's method to obtain 24-bit color image
  // cout << "Tonemaping using Drago's method ... ";

  Mat ldrDrago;
  Ptr<TonemapDrago> tonemapDrago = createTonemapDrago(1.0, 0.7);
  tonemapDrago->process(hdrDebevec, ldrDrago);
  clock_gettime(CLOCK_MONOTONIC, &tp4);

  // ldrDrago = 3 * ldrDrago;
  // imwrite("ldr-Drago.jpg", ldrDrago * 255);
  // cout << "saved ldr-Drago.jpg"<< endl;

  // Tonemap using Durand's method obtain 24-bit color image
  // cout << "Tonemaping using Durand's method ... ";

  Mat ldrDurand;
  Ptr<TonemapDurand> tonemapDurand = createTonemapDurand(1.5,4,1.0,1,1);
  tonemapDurand->process(hdrDebevec, ldrDurand);
  clock_gettime(CLOCK_MONOTONIC, &tp5);

  // ldrDurand = 3 * ldrDurand;
  // imwrite("ldr-Durand.jpg", ldrDurand * 255);
  // cout << "saved ldr-Durand.jpg"<< endl;

  // Tonemap using Reinhard's method to obtain 24-bit color image
  // cout << "Tonemaping using Reinhard's method ... ";

  Mat ldrReinhard;
  Ptr<TonemapReinhard> tonemapReinhard = createTonemapReinhard(1.5, 0,0,0);
  tonemapReinhard->process(hdrDebevec, ldrReinhard);
  clock_gettime(CLOCK_MONOTONIC, &tp6);

  // imwrite("ldr-Reinhard.jpg", ldrReinhard * 255);
  // cout << "saved ldr-Reinhard.jpg"<< endl;

  // Tonemap using Mantiuk's method to obtain 24-bit color image
  // cout << "Tonemaping using Mantiuk's method ... ";

  Mat ldrMantiuk;
  Ptr<TonemapMantiuk> tonemapMantiuk = createTonemapMantiuk(2.2,0.85, 1.2);
  tonemapMantiuk->process(hdrDebevec, ldrMantiuk);
  clock_gettime(CLOCK_MONOTONIC, &tp7);

  // ldrMantiuk = 3 * ldrMantiuk;
  // imwrite("ldr-Mantiuk.jpg", ldrMantiuk * 255);
  // cout << "saved ldr-Mantiuk.jpg"<< endl;

  printf ("AlignMTB         -> tdiff=%lf ms \n", tdiff_calc(tp0, tp1));
  printf ("CalibrateDebevec -> tdiff=%lf ms \n", tdiff_calc(tp1, tp2));
  printf ("MergeDebevec     -> tdiff=%lf ms \n", tdiff_calc(tp2, tp3));
  printf ("TonemapDrago     -> tdiff=%lf ms \n", tdiff_calc(tp3, tp4));
  printf ("TonemapDurand    -> tdiff=%lf ms \n", tdiff_calc(tp4, tp5));
  printf ("TonemapReinhard  -> tdiff=%lf ms \n", tdiff_calc(tp5, tp6));
  printf ("TonemapMantiuk   -> tdiff=%lf ms \n", tdiff_calc(tp6, tp7));
}

int main()
{
  printf("--- First Call  ---\n");
  run();
  printf("--- Second Call ---\n");
  run();

  return 0;
}
