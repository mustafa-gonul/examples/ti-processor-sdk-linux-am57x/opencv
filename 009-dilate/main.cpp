#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/ocl.hpp>

#include <time.h>
#include <unistd.h>

#include <iostream>
#include <string>

/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

using namespace cv;

bool run(const std::string& exename, const std::string& filename)
{
  UMat img;
  imread(filename, 1).copyTo(img);
  if (img.empty()) {
    std::cout << "No image data!" << std::endl;
    return false;
  }

  UMat gray, converted;
  struct timespec tp0, tp1, tp2, tp3, tp4;

  clock_gettime(CLOCK_MONOTONIC, &tp0);
  // Converting color
  cvtColor(img, gray, COLOR_RGB2GRAY);
  clock_gettime(CLOCK_MONOTONIC, &tp1);
  // dilate - 3 (copy)
  dilate(gray, converted, getStructuringElement(MORPH_RECT, Size(3, 3)));
  clock_gettime(CLOCK_MONOTONIC, &tp2);
  // dilate - 5 (copy)
  dilate(gray, converted, getStructuringElement(MORPH_RECT, Size(5, 5)));
  clock_gettime(CLOCK_MONOTONIC, &tp3);
  // dilate - 7 (copy)
  dilate(gray, converted, getStructuringElement(MORPH_RECT, Size(7, 7)));
  clock_gettime(CLOCK_MONOTONIC, &tp4);


  printf ("cvtColor          -> tdiff=%lf ms \n", tdiff_calc(tp0, tp1));
  printf ("dilate - 3 (copy) -> tdiff=%lf ms \n", tdiff_calc(tp1, tp2));
  printf ("dilate - 5 (copy) -> tdiff=%lf ms \n", tdiff_calc(tp2, tp3));
  printf ("dilate - 7 (copy) -> tdiff=%lf ms \n", tdiff_calc(tp3, tp4));


  imwrite("output.jpg", converted);

  return true;
}

int main(int argc, const char** argv)
{
  std::string exename = argv[0];
  if ( argc < 2 ) {
    std::cout << "usage: " << exename << " <Image_Path>" << std::endl;
    return -1;
  }

  std::string filename = argv[1];

  printf("--- First Call  ---\n");
  run(exename, filename);
  printf("--- Second Call ---\n");
  run(exename, filename);

  return 0;
}
