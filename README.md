# TI Processor SDK Linux - OpenCV Examples

The OpenCV features and functionality is tested on Beagleboard X15. You can download the related SDK from [here](http://www.ti.com/tool/PROCESSOR-SDK-AM57X). Currently, examples are compiled with **6th** version of SDK, I believe we can use the examples also for other versions of SDK. In the SDK [documentation](http://software-dl.ti.com/processor-sdk-linux/esd/docs/latest/linux/index.html) there is a section for [OpenCV](http://software-dl.ti.com/processor-sdk-linux/esd/docs/latest/linux/Foundational_Components_OpenCV.html). For further information please check it.

## Purpose

- Testing offloading feature of the SDK.
- Check if the functionality can be offloaded to DSP.
- Measure the performance.

## How to Use

- Edit the **config.sh** file to set the IP of your Beagleboard X15.
- Copy the [000-template](#000-template) example.
- Please delete the **build** directory if you build once the template example on your side.
- Give a proper index and name.
- Make your changes in the **main.cpp** file.
- Run **pass.sh** script in the **example directory**.
- If you want to run all the examples, run **pass.sh** script in the **project directory**.
- If you want to write all the outputs to a separate txt file, you can run **report.sh** script in the **project directory**. It creates **report.txt** file.

## Example Structure

- **main.cpp**: The main cpp file.
- **pass.sh**: One can build the example with the script.
- **images (Optional)**:
  - **Case 1, if there is no images directory in the example:**

    If you don't create any images in the **images** directory, example is executed with the default images which are in the **images** directory which is also in the **project directory**. All the images are executed by the example one by one passing the one image as a parameter to the example.

  - **Case 2, if there is an images directory in the example:**

    If you have special example needs special images, you should create and **image** directory to the example directory. In this case all the images are passed to the example as parameters, the example is executed once with the images.

## Project Structure

- **config.sh**: Project configuration script. Currently, there is only one configuration which is Beagleboard X15 IP. It is assumed that a reliable SSH connection can be established through your network to your Beagleboard X15.
- **pass.sh**: You can build and run all the examples with the script. It dumps the messages from examples to **stdout**.
- **report.sh**: You can build and run all the examples with the script. It dumps the messages from examples to **report.txt** file and **stdout**. **tee** is necessary to run the script.
- **images/\***: The project wide images reside in the directory.
- **common/pass-all.sh**: If there is a custom **images** directory in the example, this script is copied to the target as **run.sh** script. The script passes all the image file names as the parameters to the application at once.
- **common/pass-each.sh**: If there is **no custom images** directory in the example, this script is copied to the target as **run.sh** script. The script runs the application for each image file
- **common/pass.sh**: Common pass script. **pass.sh** scripts in the examples actually sourcing this script.

## Filters

I have created this section because most of the time I have a problem with understanding and explaining the OpenCV filter names to other people in the project.

| OpenCV Name | Well Known Explanation |
|-------------|------------------------|
| boxFilter   | Mean filter            |
| dilate      | Max filter             |
| erode       | Min filter             |
| medianBlur  | Median filter          |
| pyrDown     | Down Sampling          |
| pyrUp       | Up sampling            |
| sober       | Sober                  |

## Offloading

The functions cannot be offloaded to DSP cores directly. The following environmental variables should be created before running the application. You can read further information from [here](http://software-dl.ti.com/processor-sdk-linux/esd/docs/latest/linux/Foundational_Components_OpenCV.html#opencl-offload).

```bash
export TI_OCL_KEEP_FILES=Y
export TI_OCL_LOAD_KERNELS_ONCHIP=Y
export TI_OCL_CACHE_KERNELS=Y
export OPENCV_OPENCL_DEVICE='TI AM57:ACCELERATOR:TI Multicore C66 DSP'
```

If you want to disable the offloading you can do the following:

```bash
export OPENCV_OPENCL_DEVICE='disabled'
```

You can also enable offloading programmatically in the code:

```cpp
#include <opencv2/core/ocl.hpp>

...

cv::ocl::setUseOpenCL(true); /* resume DSP dispatch - from now on kernels, based on above decision tree, can be dispatched to DSP */
```

Please also check [pass-all.sh](common/pass-all.sh) and [pass-each.sh](common/pass-each.sh) scripts in the project.

## Problems & Related Questions in the TI Forum

### Building the OpenCV functions on the target

One of the main problem is, when you start to offload opencv functions, related kernels should be compiled at least once. (I assume you are caching the compiled kernels.) I have observed that passing different parameters to the functions can trigger the compilation even if you are caching the kernels. Calling all the functions in the beginning of the application that you will use, can be a solution/workaround to the problem, unfortunately the solution increases the boot time of the application.

Please see the related question in the TI Forum: [Embedding OpenCL kernels in executable](https://e2e.ti.com/support/processors/f/791/t/839436). The real solution needs a patch on TI OpenCV.

### Erode & Dilate Problem

#### Case 1

Currently, I am using SDK version 06.00.00.07 and the following code cannot be offloaded to the target due to the compilation errors on the target.

```cpp
Mat element = getStructuringElement(MORPH_RECT, Size(7, 7));

clock_gettime(CLOCK_MONOTONIC, &tp0);
erode(img, gray, element);
clock_gettime(CLOCK_MONOTONIC, &tp1);
dilate(img, gray, element);
clock_gettime(CLOCK_MONOTONIC, &tp2);
```

Please see the related question in the TI Forum: [OpenCV compilation error](https://e2e.ti.com/support/processors/f/791/t/838889). While writing this, a new version of SDK is released. Most probably it the new version problem is solved. In any case, calling OpenCL min/max functions are wrong, somehow DSP intrinsics should be called.

#### Case 2

Another problem with **erode** and **dilate** filter is, you need a structuring element and it is size cannot be greated than 8 if you want to make offloading. If you use more than eight as a size in the function, it will be executed in the ARM cores. OpenCL kernels written for these functions only supports size 8x8. Please check the following code:

```cpp
Mat element = getStructuringElement(MORPH_RECT, Size(8, 8));
```

### Limited support & Writing Own Kernels

The biggest problem, there is no enough offloading capability in the TI's OpenCV. Several features can be offloaded, if you want to go with other features like HDR, you need to write OpenCL versions of the OpenCV functions. Please check: [Necessary steps to modify OpenCV framework to add more OpenCL Host side and DSP C66 optimized kernels](http://software-dl.ti.com/processor-sdk-linux/esd/docs/latest/linux/Foundational_Components_OpenCV.html#necessary-steps-to-modify-opencv-framework-to-add-more-opencl-host-side-and-dsp-c66-optimized-kernels).

## Examples

### 000-template

If you want to create an example, you can start with copying this example as a boilerplate.

### 001-canny

TBD

### 002-erode-dilate-error

The example is created to test **erode** and **dilate**. The other name of the filter is **min filter** and **max filter**. Please check [here](#erode--dilate-problem).

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 003-convert-image-gray

The color (3 channel) image is converted to gray (1 channel) one.

**References:**

- [Color Space Conversions](https://docs.opencv.org/master/d8/d01/group__imgproc__color__conversions.html)

### 004-box-filter

The example is created to test the **box filter**. The other name of the filter is **mean filter**.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 005-median-blur

The example is created to test **median blur** The other name of the filter is **median filter** only.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 006-pyr-down

The example is created to test **pyramid down**. The other name of the filter is **down sampling filter**.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 007-pyr-up

The example is created to test **pyramid up**. The other name of the filter is **up sampling filter**.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 008-erode

The example is created to test **erode**. The other name of the filter is **min filter**.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 009-dilate

The example is created to test **dilate**. The other name of the filter is **max filter**.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 010-hdr-color

The example is based on the [High Dynamic Range (HDR) Imaging using OpenCV (C++/Python)](https://www.learnopencv.com/high-dynamic-range-hdr-imaging-using-opencv-cpp-python/). Thanks to [Satya Mallick](https://www.linkedin.com/in/satyamallick/).

**References:**

-[High Dynamic Range (HDR) Imaging using OpenCV (C++/Python)](https://www.learnopencv.com/high-dynamic-range-hdr-imaging-using-opencv-cpp-python/)

### 010-hdr-gray

The example is based on the [High Dynamic Range (HDR) Imaging using OpenCV (C++/Python)](https://www.learnopencv.com/high-dynamic-range-hdr-imaging-using-opencv-cpp-python/). Thanks to [Satya Mallick](https://www.linkedin.com/in/satyamallick/).

**References:**

-[High Dynamic Range (HDR) Imaging using OpenCV (C++/Python)](https://www.learnopencv.com/high-dynamic-range-hdr-imaging-using-opencv-cpp-python/)

## References

- [TI Processor SDK AM57X](http://www.ti.com/tool/PROCESSOR-SDK-AM57X)
- [TI Processor SDK AM57X Linux Documentation](http://software-dl.ti.com/processor-sdk-linux/esd/docs/latest/linux/index.html)
- [TI Processor SDK AM57X Linux Documentation - OpenCV](http://software-dl.ti.com/processor-sdk-linux/esd/docs/latest/linux/Foundational_Components_OpenCV.html)
