#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/ocl.hpp>

#include <time.h>
#include <unistd.h>

#include <iostream>
#include <string>

/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

using namespace cv;

bool run(const std::string& exename, const std::string& filename)
{
  UMat img;
  imread(filename, 1).copyTo(img);
  if (img.empty()) {
    std::cout << "No image data!" << std::endl;
    return false;
  }

  UMat gray;
  struct timespec tp0, tp1, tp2, tp3;

  clock_gettime(CLOCK_MONOTONIC, &tp0);
  // First
  clock_gettime(CLOCK_MONOTONIC, &tp1);
  // Second
  clock_gettime(CLOCK_MONOTONIC, &tp2);
  // Third
  clock_gettime(CLOCK_MONOTONIC, &tp3);

  printf ("First  -> tdiff=%lf ms \n", tdiff_calc(tp0, tp1));
  printf ("Second -> tdiff=%lf ms \n", tdiff_calc(tp1, tp2));
  printf ("Third  -> tdiff=%lf ms \n", tdiff_calc(tp2, tp3));

  imwrite("output.jpg", gray);

  return true;
}

int main(int argc, const char** argv)
{
  std::string exename = argv[0];
  if ( argc < 2 ) {
    std::cout << "usage: " << exename << " <Image_Path>" << std::endl;
    return -1;
  }

  std::string filename = argv[1];

  printf("--- First Call  ---\n");
  run(exename, filename);
  printf("--- Second Call ---\n");
  run(exename, filename);

  return 0;
}

